package com.yhy.admin.dao;

import com.yhy.admin.dto.SysRoleDTO;
import com.yhy.admin.vo.mng.MenuMngVO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.mng.SysRoleMngVO;
import com.yhy.common.dao.BaseMngDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-9 下午3:18 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "sysRoleMngDao")
public interface SysRoleMngDao extends BaseMngDao<SysRoleDTO,SysRoleMngVO> {

    List<SysRoleMngVO> findRoleByUserInfo(@Param("sysOwnerCpy")String sysOwnerCpy, @Param("userAccount")String userAccount);

    List<OrgSetMngVO> findOrgSetByRoleids(@Param("roleids")Set<String>  roleids, @Param("menuId")String menuId);

    List<HashMap> findAllPrivList(@Param("cpyCode")String cpyCode, @Param("userAccount")String userAccount);

}
