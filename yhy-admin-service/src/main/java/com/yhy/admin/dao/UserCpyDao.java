package com.yhy.admin.dao;

import com.yhy.admin.vo.UserCpyVO;
import com.yhy.common.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-7-3 下午4:35 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Mapper
@Component(value = "userCpyDao")
public interface UserCpyDao extends BaseDao<UserCpyVO> {

    int deleteByUserAccount(@Param("userAccount") String userAccount);

    List<UserCpyVO> findUserCpyByUserAccount(String userAccount);

}
