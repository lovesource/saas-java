package com.yhy.admin.api.impl;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-2-26 下午4:45 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.yhy.admin.api.IAdminService;
import com.yhy.admin.dto.DataDictApiDTO;
import com.yhy.admin.dto.OrgSetApiDTO;
import com.yhy.admin.dto.RoleApiDTO;
import com.yhy.admin.service.SysRoleFormService;
import com.yhy.admin.service.SysRoleService;
import com.yhy.admin.service.SysRoleUserService;
import com.yhy.admin.service.UserMgrService;
import com.yhy.admin.service.mng.*;
import com.yhy.admin.vo.UserMgrVO;
import com.yhy.admin.vo.mng.DataDictMngVO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.vo.SysAttachment;
import com.yhy.common.vo.SysUser;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Transactional
@Service(IAdminService.SERVICE_BEAN)
public class AdminServiceApi implements IAdminService {


    @Autowired
    private SysUserMngService sysUserMngService;
    @Autowired
    private UserMgrService userMgrService;
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleMngService sysRoleMngService;
    @Autowired
    private SysRoleFormService sysRoleFormService;
    @Autowired
    private SysRoleUserService sysRoleUserService;
    @Autowired
    private OrgSetMngService orgSetMngService;

    @Autowired
    private DataDictMngService dataDictMngService;
    @Autowired
    private FtpManageService ftpManageService;

    @Override
    public DataDictApiDTO getDataDictByTypeAndCode(String dictType, String dictCode, String lanCode) {
        List<DataDictMngVO> rtnList = dataDictMngService.getDictByType(dictType, lanCode);
        DataDictMngVO tmpData = rtnList.stream().filter(tmpvo ->tmpvo.getDictCode().equals(dictCode)).findFirst().get();
        return JsonUtils.tranObject(tmpData,DataDictApiDTO.class);
    }

    @Override
    public SysUser getUserByUserAccountAndCpyCode(String userAccount, String cpyCode) {
        SysUser sysUser = sysUserMngService.findByUserCpyCode(userAccount, cpyCode);
        if(sysUser != null) {
            UserMgrVO userMgrVO = userMgrService.findMgrByUserAccount(cpyCode,userAccount);
            if(userMgrVO != null)
                sysUser.setMgrAccount(userMgrVO.getMgrAccount());
        }
        return sysUser;
    }

    @Override
    public RoleApiDTO getRoleByRoleId(String roleId) {
        return JsonUtils.tranObject(sysRoleService.findById(roleId),RoleApiDTO.class);
    }


    @Override
    public List<SysUser> getUserByRoleId(String roleId) {
        List<SysUserMngVO> sysUsers = sysRoleUserService.findUserByRoleId(roleId);
        return JsonUtils.tranList(sysUsers,SysUser.class);
    }

    @Override
    public Set<String> getRoleFromByUserAccount(String userAccount, String cpyCode)  {
        Set<String> roleids = sysRoleMngService.findRoleByUserInfo(cpyCode,userAccount);
        if(CollectionUtils.isEmpty(roleids)) {
            return Sets.newHashSet();
        }
        return sysRoleFormService.findFormByRoleIds(roleids);
    }

    @Override
    public Map<String,OrgSetApiDTO> getOrgsetByCode(Set<String> orgCodes) {
        if(CollectionUtils.isEmpty(orgCodes)) {
            return Maps.newHashMap();
        }
        Map<String,OrgSetApiDTO> map = new HashMap<>(orgCodes.size());
        List<OrgSetMngVO> orgSetMngVOList = orgSetMngService.getOrgsetByCode(orgCodes);
        orgSetMngVOList.stream().forEach(tmpvo -> {
            map.put(tmpvo.getOrgCode(),JsonUtils.tranObject(tmpvo, OrgSetApiDTO.class));
        });
        return map;
    }

    @Override
    public Boolean upload(SysAttachment sysAttachment, MultipartFile file, Boolean isUploadFtp) {
        return ftpManageService.upload(sysAttachment, file, isUploadFtp);
    }

}
