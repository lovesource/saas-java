package com.yhy.admin.dto;

import com.yhy.admin.vo.mng.CpyMngVO;
import com.yhy.admin.vo.CpyVO;
import com.yhy.common.dto.BaseMngDTO;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午10:17 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */

public class CpyDTO extends BaseMngDTO<CpyMngVO> {


    @Override
    public CpyVO getBusMain() {
        return new CpyVO();
    }


}
