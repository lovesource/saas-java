package com.yhy.admin.service;

import com.yhy.admin.dao.DataDictDao;
import com.yhy.admin.vo.DataDictVO;
import com.yhy.common.service.BaseMainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-26 下午1:57 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DataDictService extends BaseMainService<DataDictVO> {

    @Autowired
    private DataDictDao dataDictDao;

    @Override
    protected DataDictDao getDao() {
        return dataDictDao;
    }


    public List<DataDictVO> getDictByType(String dictType, String lanCode) {
        DataDictVO paramBean = new DataDictVO();
        paramBean.setDictType(dictType);
        paramBean.setLanCode(lanCode);
        paramBean.setEnableFlag("Y");
        return getDao().findBy(paramBean);
    }

}
