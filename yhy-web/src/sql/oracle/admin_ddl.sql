/*==============================================================*/
/* Table: t_sys_data_dict                                       */
/*==============================================================*/
create table t_sys_data_dict
(
   id                   varchar2(40)                   not null,
   dict_type            varchar2(30)                   not null,
   lan_code             varchar2(10)                   null,
   dict_code            varchar2(30)                  null,
   dict_name            varchar2(100)                  null,
   sort                 INTEGER                 null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   attribute3           varchar2(100)                  null,
   attribute4           varchar2(100)                  null,
   attribute5           varchar2(100)                  null,
   attribute6           varchar2(100)                  null,
   attribute7           varchar2(100)                  null,
   attribute8           varchar2(100)                  null,
   constraint PK_SYS_DATA_DICT primary key (id)
);

comment on table t_sys_data_dict is
'数据字典表';

comment on column t_sys_data_dict.id is
'主键(sys_code+UUID)';

comment on column t_sys_data_dict.dict_type is
'类别';

comment on column t_sys_data_dict.lan_code is
'语言';

comment on column t_sys_data_dict.dict_code is
'编码';

comment on column t_sys_data_dict.dict_name is
'名称';

comment on column t_sys_data_dict.last_update_date is
'最后更新时间';

comment on column t_sys_data_dict.last_update_by is
'最后更新人';

comment on column t_sys_data_dict.sys_org_code is
'用户所属组织部门';

comment on column t_sys_data_dict.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_data_dict.rmk is
'备注';

comment on column t_sys_data_dict.enable_flag is
'有效标识';

comment on column t_sys_data_dict.attribute1 is
'扩展属性1 ';

comment on column t_sys_data_dict.attribute2 is
'扩展属性二';

comment on column t_sys_data_dict.version is
'更新版本号';

create unique index SYS_DATA_DICT_U01 on T_SYS_DATA_DICT (DICT_TYPE, DICT_CODE, LAN_CODE);


/*==============================================================*/
/* Table: t_sys_cpy                                             */
/*==============================================================*/
create table t_sys_cpy
(
   id                   varchar2(40)                   not null,
   cpy_name             varchar2(100)                  not null,
   cpy_long_name        varchar2(200)                  null,
   cpy_type             varchar2(10)                   not null,
   cpy_code             varchar2(100)                  null,
   cer_code             varchar2(100)                  null,
   owner_app            varchar2(10)                   null,
   cpy_status           varchar2(10)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_CPY primary key (id)
);

comment on table t_sys_cpy is
'公司';

comment on column t_sys_cpy.id is
'主键(sys_code+UUID)';

comment on column t_sys_cpy.cpy_name is
'组织名称';

comment on column t_sys_cpy.cpy_long_name is
'完整名称';

comment on column t_sys_cpy.cpy_type is
'类别(公司，部门，员工)';

comment on column t_sys_cpy.cpy_code is
'组织编号（系统ID+自动编码）';

comment on column t_sys_cpy.cer_code is
'主要证书编号';

comment on column t_sys_cpy.owner_app is
'所属应用app';

comment on column t_sys_cpy.cpy_status is
'组织状态';

comment on column t_sys_cpy.last_update_date is
'最后更新时间';

comment on column t_sys_cpy.last_update_by is
'最后更新人';

comment on column t_sys_cpy.rmk is
'备注';

comment on column t_sys_cpy.enable_flag is
'有效标识';

comment on column t_sys_cpy.attribute1 is
'扩展属性1 ';

comment on column t_sys_cpy.attribute2 is
'扩展属性二';

comment on column t_sys_cpy.version is
'更新版本号';

-- Create/Recreate indexes
create unique index SYS_CPY_CODE_U01 on T_SYS_CPY (cpy_code);

alter table T_SYS_CPY add SYS_ORG_CODE VARCHAR2(40);
alter table T_SYS_CPY add SYS_OWNER_CPY VARCHAR2(40);
-- Add comments to the columns
comment on column T_SYS_CPY.SYS_ORG_CODE
  is '用户所属组织部门';
comment on column T_SYS_CPY.SYS_OWNER_CPY
  is '用户所属公司';



/*==============================================================*/
/* Table: t_sys_user                                            */
/*==============================================================*/
create table t_sys_user
(
   id                   varchar2(40)                   not null,
   user_name            varchar2(100)                  not null,
   user_account         varchar2(40)                   not null,
   password             varchar2(40)                   null,
   phone                varchar2(100)                  null,
   email                varchar2(100)                  null,
   lan_code             varchar2(10)                   null,
   cpy_code             varchar2(40)                   null,
   org_code             varchar2(40)                   null,
   avatar               varchar2(200)                  null,
   weixin               varchar2(100)                  null,
   user_status          varchar2(20)                   null,
   third_account        varchar2(50)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_USER primary key (id)
);

comment on table t_sys_user is
'系统用户';

comment on column t_sys_user.id is
'主键(sys_code+UUID)';

comment on column t_sys_user.user_name is
'用户名';

comment on column t_sys_user.user_account is
'账号';

comment on column t_sys_user.password is
'密码(密码+用户名 MD5加密串)';

comment on column t_sys_user.phone is
'手机号';

comment on column t_sys_user.email is
'邮件地址';

comment on column t_sys_user.lan_code is
'默认语言';

comment on column t_sys_user.cpy_code is
'所属组织部门ID';

comment on column t_sys_user.org_code is
'所属组织部门编码';

comment on column t_sys_user.avatar is
'头像';

comment on column t_sys_user.weixin is
'微信';

comment on column t_sys_user.user_status is
'用户状态(Lock:锁定,UnLock:解锁)';

comment on column t_sys_user.last_update_date is
'最后更新时间';

comment on column t_sys_user.last_update_by is
'最后更新人';

comment on column t_sys_user.sys_org_code is
'用户所属组织部门';

comment on column t_sys_user.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_user.rmk is
'备注';

comment on column t_sys_user.enable_flag is
'有效标识';

comment on column t_sys_user.attribute1 is
'扩展属性1 ';

comment on column t_sys_user.attribute2 is
'扩展属性二';

comment on column t_sys_user.version is
'更新版本号';

-- Create/Recreate primary, unique and foreign key constraints
alter table T_SYS_USER
  add constraint SYS_USER_ACCOUNT_U01 unique (USER_ACCOUNT);


/*==============================================================*/
/* Table: t_sys_user_mgr                                        */
/*==============================================================*/
create table t_sys_user_mgr
(
   id                   varchar2(40)                   not null,
   user_id              varchar2(40)                   not null,
   user_account         varchar2(40)                   not null,
   mgr_account          varchar2(40)                   null,
   cpy_code             varchar2(40)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          char(1)                        null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_USER_MGR primary key (id)
);

comment on table t_sys_user_mgr is
'用户对应上司';

comment on column t_sys_user_mgr.id is
'主键(sys_code+UUID)';

comment on column t_sys_user_mgr.user_id is
'用户ID';

comment on column t_sys_user_mgr.user_account is
'账号';

comment on column t_sys_user_mgr.mgr_account is
'上级用户账号';

comment on column t_sys_user_mgr.cpy_code is
'公司 ID（对应组织ID）';

comment on column t_sys_user_mgr.last_update_date is
'最后更新时间';

comment on column t_sys_user_mgr.last_update_by is
'最后更新人';

comment on column t_sys_user_mgr.sys_org_code is
'用户所属组织部门';

comment on column t_sys_user_mgr.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_user_mgr.rmk is
'备注';

comment on column t_sys_user_mgr.enable_flag is
'有效标识';

comment on column t_sys_user_mgr.attribute1 is
'扩展属性1 ';

comment on column t_sys_user_mgr.attribute2 is
'扩展属性二';

comment on column t_sys_user_mgr.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_user_cpy                                        */
/*==============================================================*/
create table t_sys_user_cpy
(
   id                   varchar2(40)                   not null,
   user_id              varchar2(40)                   not null,
   user_account         varchar2(40)                   not null,
   cpy_code             varchar2(40)                   not null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_USER_CPY primary key (id)
);

comment on table t_sys_user_cpy is
'用户所属公司';

comment on column t_sys_user_cpy.id is
'主键(sys_code+UUID)';

comment on column t_sys_user_cpy.user_id is
'用户ID';

comment on column t_sys_user_cpy.user_account is
'账号';

comment on column t_sys_user_cpy.cpy_code is
'公司 ID（对应组织ID）';

comment on column t_sys_user_cpy.last_update_date is
'最后更新时间';

comment on column t_sys_user_cpy.last_update_by is
'最后更新人';

comment on column t_sys_user_cpy.sys_org_code is
'用户所属组织部门';

comment on column t_sys_user_cpy.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_user_cpy.rmk is
'备注';

comment on column t_sys_user_cpy.enable_flag is
'有效标识';

comment on column t_sys_user_cpy.attribute1 is
'扩展属性1 ';

comment on column t_sys_user_cpy.attribute2 is
'扩展属性二';

comment on column t_sys_user_cpy.version is
'更新版本号';

-- Create/Recreate indexes
create index SYS_USER_CPY_ACCUNT_IDX01 on T_SYS_USER_CPY (user_account);


/*==============================================================*/
/* Table: t_sys_org_set                                         */
/*==============================================================*/
create table t_sys_org_set
(
   id                   varchar2(40)                   not null,
   org_name             varchar2(200)                  not null,
   org_type             varchar2(10)                   not null,
   parent_id            varchar2(40)                   null,
   org_code             varchar2(100)                  null,
   org_status           varchar2(2)                    null,
   cpy_code             varchar2(40)                   null,
   third_code           varchar2(50)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_ORG_SET primary key (id)
);

comment on table t_sys_org_set is
'组织机构(部门)';

comment on column t_sys_org_set.id is
'主键(sys_code+UUID)';

comment on column t_sys_org_set.org_name is
'组织名称';

comment on column t_sys_org_set.org_type is
'类别(公司，部门，员工)';

comment on column t_sys_org_set.parent_id is
'上级菜单';

comment on column t_sys_org_set.org_code is
'组织编号（系统ID+自动编码）';

comment on column t_sys_org_set.org_status is
'组织状态';

comment on column t_sys_org_set.third_code is
'第三方编号';

comment on column t_sys_org_set.last_update_date is
'最后更新时间';

comment on column t_sys_org_set.last_update_by is
'最后更新人';

comment on column t_sys_org_set.rmk is
'备注';

comment on column t_sys_org_set.enable_flag is
'有效标识';

comment on column t_sys_org_set.attribute1 is
'扩展属性1 ';

comment on column t_sys_org_set.attribute2 is
'扩展属性二';

comment on column t_sys_org_set.version is
'更新版本号';

alter table t_sys_org_set add mgr_account varchar2(50);
comment on column t_sys_org_set.mgr_account is '机构主要负责人';

-- Create/Recreate primary, unique and foreign key constraints
alter table T_SYS_ORG_SET
  add constraint SYS_ORG_SET_CODE_U01 unique (ORG_CODE);
-- Create/Recreate indexes
create index Sys_ORG_SET_CPY_IDX01 on T_SYS_ORG_SET (cpy_code);

alter table t_sys_org_set add SYS_ORG_CODE VARCHAR2(40);
alter table t_sys_org_set add SYS_OWNER_CPY VARCHAR2(40);
-- Add comments to the columns
comment on column t_sys_org_set.SYS_ORG_CODE
  is '用户所属组织部门';
comment on column t_sys_org_set.SYS_OWNER_CPY
  is '用户所属公司';



/*==============================================================*/
/* Table: t_sys_user_org_set                                    */
/*==============================================================*/
create table t_sys_user_org_set
(
   id                   varchar2(40)                   not null,
   user_id              varchar2(40)                   not null,
   user_account         varchar2(40)                   not null,
   org_code             varchar2(100)                  null,
   cpy_code              varchar2(40)                  null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_USER_ORG_SET primary key (id)
);

comment on table t_sys_user_org_set is
'用户所属组织部门';

comment on column t_sys_user_org_set.id is
'主键(sys_code+UUID)';

comment on column t_sys_user_org_set.user_id is
'用户ID';

comment on column t_sys_user_org_set.user_account is
'账号';

comment on column t_sys_user_org_set.org_code is
'组织代码(通常对应公司部门代码 ）';

comment on column t_sys_user_org_set.cpy_code is
'公司 ID（对应组织ID）';

comment on column t_sys_user_org_set.last_update_date is
'最后更新时间';

comment on column t_sys_user_org_set.last_update_by is
'最后更新人';

comment on column t_sys_user_org_set.sys_org_code is
'用户所属组织部门';

comment on column t_sys_user_org_set.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_user_org_set.rmk is
'备注';

comment on column t_sys_user_org_set.enable_flag is
'有效标识';

comment on column t_sys_user_org_set.attribute1 is
'扩展属性1 ';

comment on column t_sys_user_org_set.attribute2 is
'扩展属性二';

comment on column t_sys_user_org_set.version is
'更新版本号';

-- Create/Recreate indexes
create index SYS_USER_ORG_SET_CPY_ORG_IDX01 on T_SYS_USER_ORG_SET (org_code, cpy_code);


/*==============================================================*/
/* Table: t_sys_menu                                            */
/*==============================================================*/
create table t_sys_menu
(
   id                   varchar2(40)                   not null,
   menu_name            varchar2(200)                  not null,
   menu_type            varchar2(10)                   not null,
   parent_id            varchar2(40)                   null,
   component_url        varchar2(100)                  null,
   path_url             varchar2(100)                  null,
   sort                 number(3)                      null,
   icon                 varchar2(100)                  null,
   is_admin             char(1) default 0 not null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_MENU primary key (id)
);
-- Add/modify columns
alter table T_SYS_MENU add priv_code varchar2(50);
-- Add comments to the columns
comment on column T_SYS_MENU.priv_code
is '权限代码';

comment on table t_sys_menu is
'菜单管理';

comment on column t_sys_menu.id is
'主键(sys_code+UUID)';

comment on column t_sys_menu.menu_name is
'菜单名称';

comment on column t_sys_menu.is_admin is
'是否管理员';

comment on column t_sys_menu.menu_type is
'类别(外部，内部)';

comment on column t_sys_menu.parent_id is
'上级菜单';

comment on column t_sys_menu.component_url is
'组件路径';

comment on column t_sys_menu.path_url is
'链接地址';

comment on column t_sys_menu.sort is
'排序号';

comment on column t_sys_menu.icon is
'菜单图标';

comment on column t_sys_menu.last_update_date is
'最后更新时间';

comment on column t_sys_menu.last_update_by is
'最后更新人';

comment on column t_sys_menu.sys_org_code is
'用户所属组织部门';

comment on column t_sys_menu.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_menu.rmk is
'备注';

comment on column t_sys_menu.enable_flag is
'有效标识';

comment on column t_sys_menu.attribute1 is
'扩展属性1 ';

comment on column t_sys_menu.attribute2 is
'扩展属性二';

comment on column t_sys_menu.version is
'更新版本号';

-- Add/modify columns
alter table T_SYS_MENU add module_bus_class VARCHAR2(20);
alter table T_SYS_MENU add is_visable CHAR(1) default 1;
alter table T_SYS_MENU add is_workflow CHAR(1) default 0;
-- Add comments to the columns
comment on column T_SYS_MENU.module_bus_class
is '业务单据Code';
comment on column T_SYS_MENU.is_visable
is '菜单是否可见';
comment on column T_SYS_MENU.is_workflow
is '是否开启工作流';


/*==============================================================*/
/* Table: t_sys_menu_lan                                        */
/*==============================================================*/
create table t_sys_menu_lan
(
   id                   varchar2(40)                   null,
   menu_id              varchar2(40)                   not null,
   lan_code             varchar2(10)                   not null,
   name                 varchar2(100)                  not null,
   rmk                  varchar2(1000)                 null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_MENU_LAN primary key (menu_id, lan_code)
);

comment on table t_sys_menu_lan is
'菜单管理多语言';

comment on column t_sys_menu_lan.menu_id is
'菜单主ID';

comment on column t_sys_menu_lan.lan_code is
'菜单名称';

comment on column t_sys_menu_lan.name is
'类别';

comment on column t_sys_menu_lan.rmk is
'备注';

comment on column t_sys_menu_lan.attribute1 is
'扩展属性1 ';

comment on column t_sys_menu_lan.attribute2 is
'扩展属性二';

comment on column t_sys_menu_lan.version is
'更新版本号';

/*==============================================================*/
/* Table: t_quartz_job                                          */
/*==============================================================*/
create table t_quartz_job
(
   id                   varchar2(40)                   not null,
   job_name             varchar2(100)                  not null,
   cron_expression      varchar2(50)                   null,
   is_pause             number(1)                      null,
   is_running           number(1)                      null,
   bean_name            varchar2(100)                  null,
   method_param         varchar2(200)                  null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          varchar2(1)                    null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_QUARTZ_JOB primary key (id)
);

comment on table t_quartz_job is
'定时任务表';

comment on column t_quartz_job.id is
'主键(sys_code+UUID)';

comment on column t_quartz_job.job_name is
'job名称';

comment on column t_quartz_job.cron_expression is
'表达式';

comment on column t_quartz_job.is_pause is
'是否暂停(1:暂停,0:运行)';

comment on column t_quartz_job.is_running is
'是否正在执行(1:执行,0:未执行)';

comment on column t_quartz_job.last_update_date is
'最后更新时间';

comment on column t_quartz_job.last_update_by is
'最后更新人';

comment on column t_quartz_job.sys_org_code is
'用户所属组织部门';

comment on column t_quartz_job.sys_owner_cpy is
'用户所属公司';

comment on column t_quartz_job.rmk is
'备注';

comment on column t_quartz_job.enable_flag is
'有效标识';

comment on column t_quartz_job.attribute1 is
'扩展属性1 ';

comment on column t_quartz_job.attribute2 is
'扩展属性二';

comment on column t_quartz_job.version is
'更新版本号';


/*==============================================================*/
/* Table: t_sys_job                                             */
/*==============================================================*/
create table t_sys_job
(
   id                   varchar2(40)                   not null,
   job_name             varchar2(200)                  not null,
   job_type             varchar2(10)                   not null,
   job_code             varchar2(100)                  null,
   job_status           varchar2(2)                    null,
   cpy_code             varchar2(40)                   null,
   third_code           varchar2(50)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          char(1)                        null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_JOB primary key (id)
);

comment on table t_sys_job is
'职务管理';

comment on column t_sys_job.id is
'主键(sys_code+UUID)';

comment on column t_sys_job.job_name is
'组织名称';

comment on column t_sys_job.job_type is
'类别(公司，部门，员工)';

comment on column t_sys_job.job_code is
'组织编号（系统ID+自动编码）';

comment on column t_sys_job.job_status is
'组织状态';

comment on column t_sys_job.third_code is
'第三方编号';

comment on column t_sys_job.sys_org_code is
'用户所属组织部门';

comment on column t_sys_job.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_job.last_update_date is
'最后更新时间';

comment on column t_sys_job.last_update_by is
'最后更新人';

comment on column t_sys_job.rmk is
'备注';

comment on column t_sys_job.enable_flag is
'有效标识';

comment on column t_sys_job.attribute1 is
'扩展属性1 ';

comment on column t_sys_job.attribute2 is
'扩展属性二';

comment on column t_sys_job.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_user_job                                        */
/*==============================================================*/
create table t_sys_user_job
(
   id                   varchar2(40)                   not null,
   user_id              varchar2(40)                   not null,
   job_name             varchar2(100)                  not null,
   job_id               varchar2(40)                   null,
   cpy_code             varchar2(40)                   null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          char(1)                        null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_USER_JOB primary key (id)
);

comment on table t_sys_user_job is
'用户对应职位';

comment on column t_sys_user_job.id is
'主键(sys_code+UUID)';

comment on column t_sys_user_job.user_id is
'用户ID';

comment on column t_sys_user_job.job_name is
'职务名称';

comment on column t_sys_user_job.job_id is
'职位ID';

comment on column t_sys_user_job.cpy_code is
'公司编号';

comment on column t_sys_user_job.last_update_date is
'最后更新时间';

comment on column t_sys_user_job.last_update_by is
'最后更新人';

comment on column t_sys_user_job.sys_org_code is
'用户所属组织部门';

comment on column t_sys_user_job.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_user_job.rmk is
'备注';

comment on column t_sys_user_job.enable_flag is
'有效标识';

comment on column t_sys_user_job.attribute1 is
'扩展属性1 ';

comment on column t_sys_user_job.attribute2 is
'扩展属性二';

comment on column t_sys_user_job.version is
'更新版本号';


/*==============================================================*/
/* Table: t_sys_print_main                                      */
/*==============================================================*/
create table t_sys_print_main
(
   id                   varchar2(40)                   not null,
   report_code          varchar2(100)                  null,
   report_name          varchar2(100)                  null,
   attachment_id        varchar2(40)                   null,
   attachment_path      varchar2(200)                  null,
   report_sort          integer                        null,
   create_date          date                           not null,
   last_update_date     date                           null,
   create_by            varchar2(40)                   null,
   last_update_by       varchar2(40)                   null,
   sys_org_code         varchar2(40)                   null,
   sys_owner_cpy        varchar2(40)                   null,
   rmk                  varchar2(1000)                 null,
   enable_flag          char(1)                        null,
   attribute1           varchar2(100)                  null,
   attribute2           varchar2(100)                  null,
   version              number(8)                      null,
   constraint PK_T_SYS_PRINT_MAIN primary key (id)
);

comment on table t_sys_print_main is
'自定义报表打印格式';

comment on column t_sys_print_main.id is
'主键(sys_code+UUID)';

comment on column t_sys_print_main.report_code is
'报表编号(对应系统数据字典的print_report_type)';

comment on column t_sys_print_main.report_name is
'报表名称';

comment on column t_sys_print_main.attachment_id is
'附档ID对应 t_sys_attachment.id';

comment on column t_sys_print_main.attachment_path is
'报表sql';

comment on column t_sys_print_main.report_sort is
'排序';

comment on column t_sys_print_main.last_update_date is
'最后更新时间';

comment on column t_sys_print_main.last_update_by is
'最后更新人';

comment on column t_sys_print_main.sys_org_code is
'用户所属组织部门';

comment on column t_sys_print_main.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_print_main.rmk is
'备注';

comment on column t_sys_print_main.enable_flag is
'有效标识';

comment on column t_sys_print_main.attribute1 is
'扩展属性1 ';

comment on column t_sys_print_main.attribute2 is
'扩展属性二';

comment on column t_sys_print_main.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_role                                          */
/*==============================================================*/
create table t_sys_role
(
   id                 varchar2(40)         not null,
   role_name          varchar2(100)        not null,
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          varchar2(40),
   last_update_by     varchar2(40),
   sys_org_code       varchar2(40),
   sys_owner_cpy      varchar2(40),
   rmk                varchar2(1000),
   enable_flag        CHAR(1),
   attribute1         varchar2(100),
   attribute2         varchar2(100),
   version            number(8),
   constraint PK_SYS_ROLE primary key (id)
);

comment on table t_sys_role is
'角色表(RBAC Role-Base-Access-Control)';

comment on column t_sys_role.id is
'主键(sys_code+UUID)';

comment on column t_sys_role.role_name is
'用户名';

comment on column t_sys_role.last_update_date is
'最后更新时间';

comment on column t_sys_role.last_update_by is
'最后更新人';

comment on column t_sys_role.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role.rmk is
'备注';

comment on column t_sys_role.enable_flag is
'有效标识';

comment on column t_sys_role.attribute1 is
'扩展属性1 ';

comment on column t_sys_role.attribute2 is
'扩展属性二';

comment on column t_sys_role.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_role_user                                     */
/*==============================================================*/
create table t_sys_role_user
(
   id                 varchar2(40)         not null,
   role_id            varchar2(40)         not null,
   user_id            varchar2(40),
   user_account       varchar2(40),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          varchar2(40),
   last_update_by     varchar2(40),
   sys_org_code       varchar2(40),
   sys_owner_cpy      varchar2(40),
   rmk                varchar2(1000),
   enable_flag        CHAR(1),
   attribute1         varchar2(100),
   attribute2         varchar2(100),
   version            number(8),
   constraint PK_T_SYS_ROLE_USER primary key (id)
);

comment on table t_sys_role_user is
'角色用户表';

comment on column t_sys_role_user.id is
'主键(sys_code+UUID)';

comment on column t_sys_role_user.role_id is
'角色 ID';

comment on column t_sys_role_user.user_id is
'用户ID';

comment on column t_sys_role_user.user_account is
'用户账号';

comment on column t_sys_role_user.last_update_date is
'最后更新时间';

comment on column t_sys_role_user.last_update_by is
'最后更新人';

comment on column t_sys_role_user.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role_user.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role_user.rmk is
'备注';

comment on column t_sys_role_user.enable_flag is
'有效标识';

comment on column t_sys_role_user.attribute1 is
'扩展属性1 ';

comment on column t_sys_role_user.attribute2 is
'扩展属性二';

comment on column t_sys_role_user.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_role_menu                                     */
/*==============================================================*/
create table t_sys_role_menu
(
   id                 varchar2(40)         not null,
   role_id            varchar2(40)         not null,
   menu_id            varchar2(40),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          varchar2(40),
   last_update_by     varchar2(40),
   sys_org_code       varchar2(40),
   sys_owner_cpy      varchar2(40),
   rmk                varchar2(1000),
   enable_flag        CHAR(1),
   attribute1         varchar2(100),
   attribute2         varchar2(100),
   version            number(8),
   constraint PK_T_SYS_ROLE_MENU primary key (id)
);

comment on table t_sys_role_menu is
'角色菜单关系表(所能操作的菜单)';

comment on column t_sys_role_menu.id is
'主键(sys_code+UUID)';

comment on column t_sys_role_menu.role_id is
'角色 ID';

comment on column t_sys_role_menu.menu_id is
'菜单ID(对应下级菜单ID)';

comment on column t_sys_role_menu.last_update_date is
'最后更新时间';

comment on column t_sys_role_menu.last_update_by is
'最后更新人';

comment on column t_sys_role_menu.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role_menu.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role_menu.rmk is
'备注';

comment on column t_sys_role_menu.enable_flag is
'有效标识';

comment on column t_sys_role_menu.attribute1 is
'扩展属性1 ';

comment on column t_sys_role_menu.attribute2 is
'扩展属性二';

comment on column t_sys_role_menu.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_role_org                                      */
/*==============================================================*/
create table t_sys_role_org
(
   id                 varchar2(40)         not null,
   role_id            varchar2(40)         not null,
   org_id             varchar2(40),
   org_code           varchar2(40),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          varchar2(40),
   last_update_by     varchar2(40),
   sys_org_code       varchar2(40),
   sys_owner_cpy      varchar2(40),
   rmk                varchar2(1000),
   enable_flag        CHAR(1),
   attribute1         varchar2(100),
   attribute2         varchar2(100),
   version            number(8),
   constraint PK_T_SYS_ROLE_ORG primary key (id)
);

comment on table t_sys_role_org is
'角色组织关系表(所能操作的部门)(若无则表示所有)';

comment on column t_sys_role_org.id is
'主键(sys_code+UUID)';

comment on column t_sys_role_org.role_id is
'角色 ID';

comment on column t_sys_role_org.org_id is
'组织ID';

comment on column t_sys_role_org.org_code is
'组织编号';

comment on column t_sys_role_org.last_update_date is
'最后更新时间';

comment on column t_sys_role_org.last_update_by is
'最后更新人';

comment on column t_sys_role_org.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role_org.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role_org.rmk is
'备注';

comment on column t_sys_role_org.enable_flag is
'有效标识';

comment on column t_sys_role_org.attribute1 is
'扩展属性1 ';

comment on column t_sys_role_org.attribute2 is
'扩展属性二';

comment on column t_sys_role_org.version is
'更新版本号';

/*==============================================================*/
/* Table: t_sys_role_form                                     */
/*==============================================================*/
create table t_sys_role_form
(
   id                 varchar2(40)         not null,
   role_id            varchar2(40)         not null,
   form_code          varchar2(40),
   form_name          varchar2(40),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          varchar2(40),
   last_update_by     varchar2(40),
   sys_org_code       varchar2(40),
   sys_owner_cpy      varchar2(40),
   rmk                varchar2(1000),
   enable_flag        CHAR(1),
   attribute1         varchar2(100),
   attribute2         varchar2(100),
   version            number(8),
   constraint PK_T_SYS_ROLE_FORM primary key (id)
);

comment on table t_sys_role_form is
'角色表单关系表';

comment on column t_sys_role_form.id is
'主键(sys_code+UUID)';

comment on column t_sys_role_form.role_id is
'角色 ID';

comment on column t_sys_role_form.form_code is
'表单系统编号';

comment on column t_sys_role_form.form_name is
'表单名称';

comment on column t_sys_role_form.last_update_date is
'最后更新时间';

comment on column t_sys_role_form.last_update_by is
'最后更新人';

comment on column t_sys_role_form.sys_org_code is
'用户所属组织部门';

comment on column t_sys_role_form.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_role_form.rmk is
'备注';

comment on column t_sys_role_form.enable_flag is
'有效标识';

comment on column t_sys_role_form.attribute1 is
'扩展属性1 ';

comment on column t_sys_role_form.attribute2 is
'扩展属性二';

comment on column t_sys_role_form.version is
'更新版本号';

/*==============================================================*/
/* Index: sys_role_form_idx_roleid                            */
/*==============================================================*/
create index sys_role_form_idx_roleid on t_sys_role_form (
   role_id ASC
);
/*==============================================================*/
/* Table: t_sys_custom_field                                  */
/*==============================================================*/
create table t_sys_custom_field
(
   id                 VARCHAR2(40)         not null,
   grid_id            VARCHAR2(40),
   field_prop         VARCHAR2(40),
   field_label        VARCHAR2(60),
   field_width        NUMBER(4,0),
   field_sort         NUMBER(3,0),
   field_visible      number(1),
   bus_module         VARCHAR2(10),
   bus_class          VARCHAR2(10),
   create_date        DATE                 not null,
   last_update_date   DATE,
   create_by          VARCHAR2(40),
   last_update_by     VARCHAR2(40),
   sys_org_code       VARCHAR2(40),
   sys_owner_cpy      VARCHAR2(40),
   rmk                VARCHAR2(1000),
   enable_flag        CHAR(1),
   attribute1         VARCHAR2(100),
   attribute2         VARCHAR2(100),
   version            NUMBER(8,0),
   constraint PK_T_SYS_CUSTOM_FIELD primary key (id)
);

comment on table t_sys_custom_field is
'用户自定义字段定义';

comment on column t_sys_custom_field.id is
'主键(sys_code+UUID)';

comment on column t_sys_custom_field.grid_id is
'表单ID';

comment on column t_sys_custom_field.field_prop is
'列字段名';

comment on column t_sys_custom_field.field_label is
'列名称';

comment on column t_sys_custom_field.field_width is
'列宽度';

comment on column t_sys_custom_field.field_sort is
'字段排序';

comment on column t_sys_custom_field.field_visible is
'是否可见(1:可见，0隐藏)';

comment on column t_sys_custom_field.bus_module is
'模块';

comment on column t_sys_custom_field.bus_class is
'业务种类';

comment on column t_sys_custom_field.last_update_date is
'最后更新时间';

comment on column t_sys_custom_field.last_update_by is
'最后更新人';

comment on column t_sys_custom_field.sys_org_code is
'用户所属组织部门';

comment on column t_sys_custom_field.sys_owner_cpy is
'用户所属公司';

comment on column t_sys_custom_field.rmk is
'备注';

comment on column t_sys_custom_field.enable_flag is
'有效标识';

comment on column t_sys_custom_field.attribute1 is
'扩展属性1 ';

comment on column t_sys_custom_field.attribute2 is
'扩展属性二';

comment on column t_sys_custom_field.version is
'更新版本号';

/*==============================================================*/
/* Index: sys_custom_field_idx_mainid                         */
/*==============================================================*/
create index sys_custom_field_idx_mainid on t_sys_custom_field (
   grid_id ASC,
   create_by ASC,
   sys_owner_cpy ASC,
   enable_flag ASC
);
