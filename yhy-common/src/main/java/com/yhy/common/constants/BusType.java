package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 * 业务类别
 */
public enum BusType {

	// 单方同意
	ONE(10,"ONE"),

	// 双方同意
	TWO(20,"TWO");

	private final Integer val;
	private final String labCode;

	private static Map<Integer, BusType> busStateMap;

	BusType(Integer val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public int getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static BusType getInstByVal(Integer key) {
		synchronized (BusType.class) {
			if (busStateMap == null) {
				busStateMap = new HashMap<Integer, BusType>();
				for (BusType busState : BusType.values()) {
					busStateMap.put(busState.getVal(), busState);
				}
			}
		}
		if (!busStateMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的BusType枚举值不存在。");
		}
		return busStateMap.get(key);
	}

}
