package com.yhy.common.dto;

import com.yhy.common.utils.ToStringBean;
import com.yhy.common.vo.PageVO;

import java.io.Serializable;

/**
 * app返回信息对象
 * @author nymxj
 *
 */
public class AppReturnMsg extends ToStringBean implements Serializable {
	
	/**
	 * 操作返回码
	 */
	private String code  = "-1";
	
	/**
	 * 提示信息
	 */
	private String msg;

	/**
	 * 传递对象
	 */
	private Object data;

	/**
	 * token
	 */
	private String token;

	public AppReturnMsg() {
		
	}

	public static AppReturnMsg success(String msg, Object data) {
		return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(), msg, data);
	}

	public static AppReturnMsg fail(String msg, Object data) {
		return new AppReturnMsg(ReturnCode.FAIL_CODE.getCode(), msg, data);
	}

	public AppReturnMsg(String code, Object data) {
		this.code = code;
		this.data = data;
	}

	public AppReturnMsg(String code, String msg, Object data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public AppReturnMsg(String code, String msg, Object data, String token) {
		this.code = code;
		this.msg = msg;
		this.data = data;
		this.token = token;
	}

    public AppReturnMsg(String code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}

	public AppReturnMsg(String code) {
		this.code = code;
	}


	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void reset(){
		this.code = "-1";
		this.msg = null;
		this.data = null;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "AppReturnMsg{" +
				"code='" + code + '\'' +
				", msg='" + msg + '\'' +
				", data=" + data +
				", token='" + token + '\'' +
				'}';
	}
}
