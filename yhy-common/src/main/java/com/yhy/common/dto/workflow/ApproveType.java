package com.yhy.common.dto.workflow;

import com.yhy.common.exception.CustomRuntimeException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-5-12 上午11:34 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public enum ApproveType {

	/**
	 * 审批打回
	 */
	APPROVE_BACK("back", "cmm.appr_back"),

	/**
	 * 审批通过
	 */
	APPROVE_PASS("pass", "cmm.appr_pass"),

	/**
	 * 审批拒绝
	 */
	APPROVE_REJECT("reject", "cmm.appr_reject");  //审批撤回

	private final String val;
	private final String labCode;

	private static Map<String, ApproveType> approveTypeMap;

	ApproveType(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	/**
	 * 根据操作类型的值获取业务类型
	 * @param val
	 * @return
	 */
	public static ApproveType getInstByVal(String val) {
		synchronized (ApproveType.class) {
			if (approveTypeMap == null) {
				approveTypeMap = new HashMap<String, ApproveType>();
				for (ApproveType approveType : ApproveType.values()) {
					approveTypeMap.put(approveType.getVal(), approveType);
				}
			}
		}
		if (!approveTypeMap.containsKey(val)) {
			throw new CustomRuntimeException("审批类型值" + val + "对应的枚举值不存在。");
		}
		return approveTypeMap.get(val);
	}
}
