package com.yhy.common.vo;

import com.yhy.common.dto.BaseEntity;

import java.util.Date;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public class BusCommonState extends BaseEntity {

    private String mainId;
    private String selfState;
    private String otherState;
    private Integer busType;
    private Date busDate;
    private Integer busDirection;
    private String busModule;
    private String busClass;
    private String funProcess;
    private String relNum;
    private String encryptCode;
    private Integer forwardType;
    private String otherSysOrgCode;
    private String otherSysOwnerCpy;
    private String rmk;

    private String parentId;
    private String customCode;
    private String sysGenCode;
    private String historyFlag;
    private Integer busVersion;

    public BusCommonState() {
    }

    public Integer getBusVersion() {
        return busVersion;
    }

    public void setBusVersion(Integer busVersion) {
        this.busVersion = busVersion;
    }

    public String getSelfState() {
        return selfState;
    }

    public void setSelfState(String selfState) {
        this.selfState = selfState;
    }

    public String getOtherState() {
        return otherState;
    }

    public void setOtherState(String otherState) {
        this.otherState = otherState;
    }

    public String getBusClass() {
        return busClass;
    }

    public void setBusClass(String busClass) {
        this.busClass = busClass;
    }

    public String getHistoryFlag() {
        return historyFlag;
    }

    public void setHistoryFlag(String historyFlag) {
        this.historyFlag = historyFlag;
    }

    public String getCustomCode() {
        return customCode;
    }

    public void setCustomCode(String customCode) {
        this.customCode = customCode;
    }

    public String getSysGenCode() {
        return sysGenCode;
    }

    public void setSysGenCode(String sysGenCode) {
        this.sysGenCode = sysGenCode;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getMainId() {
        return mainId;
    }

    public void setMainId(String mainId) {
        this.mainId = mainId;
    }

    public String getRelNum() {
        return relNum;
    }

    public void setRelNum(String relNum) {
        this.relNum = relNum;
    }

    public String getEncryptCode() {
        return encryptCode;
    }

    public void setEncryptCode(String encryptCode) {
        this.encryptCode = encryptCode;
    }

    public String getOtherSysOrgCode() {
        return otherSysOrgCode;
    }

    public void setOtherSysOrgCode(String otherSysOrgCode) {
        this.otherSysOrgCode = otherSysOrgCode;
    }

    public String getOtherSysOwnerCpy() {
        return otherSysOwnerCpy;
    }

    public void setOtherSysOwnerCpy(String otherSysOwnerCpy) {
        this.otherSysOwnerCpy = otherSysOwnerCpy;
    }

    public String getRmk() {
        return rmk;
    }

    public void setRmk(String rmk) {
        this.rmk = rmk;
    }

    public Integer getBusType() {
        return busType;
    }

    public void setBusType(Integer busType) {
        this.busType = busType;
    }

    public Date getBusDate() {
        return busDate;
    }

    public void setBusDate(Date busDate) {
        this.busDate = busDate;
    }

    public Integer getBusDirection() {
        return busDirection;
    }

    public void setBusDirection(Integer busDirection) {
        this.busDirection = busDirection;
    }

    public String getBusModule() {
        return busModule;
    }

    public void setBusModule(String busModule) {
        this.busModule = busModule;
    }

    public String getFunProcess() {
        return funProcess;
    }

    public void setFunProcess(String funProcess) {
        this.funProcess = funProcess;
    }

    public Integer getForwardType() {
        return forwardType;
    }

    public void setForwardType(Integer forwardType) {
        this.forwardType = forwardType;
    }
}
