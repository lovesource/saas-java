package com.yhy.common.jxls;

import com.yhy.common.utils.ToStringBean;
import com.yhy.common.vo.BusCommonState;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-23 下午9:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
public class MyJxlsImportBean extends ToStringBean {

	private BusCommonState busCommonState = new BusCommonState();

	public BusCommonState getBusCommonState() {
		return busCommonState;
	}

	public void setBusCommonState(BusCommonState busCommonState) {
		this.busCommonState = busCommonState;
	}
}
